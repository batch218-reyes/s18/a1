/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/


			// parameter


	function getSumof5plus15 (numA, numB) {

			console.log("Displayed Sum of " + numA + " and " + numB);
			console.log(numA + numB);
		}

		getSumof5plus15(5,15);


	function getDifferenceof10minus5 (numA, numB) {

			console.log("Displayed Difference of " + numA + " and " + numB);
			console.log(numA - numB);
		}

		getDifferenceof10minus5(20,5);



	function getProductof50multiplyby10 () {

		let	numA =50;
		let numB =10;

		console.log("Displayed product  of " + numA + " and " + numB);
		console.log(numA * numB);
		}

		getProductof50multiplyby10();


	function getquotientof50divideby10 (numA, numB) {

		let	numC =50;
		let numD =10;

		console.log("Displayed quotient  of " + numC + " and " + numD);
		console.log(numC / numD);
		}




	function circleAreas (pi, radius){
		console.log("The result of getting the area of a circle with " + radius + " radius ");
		console.log(pi * (radius**2));
	}

	circleAreas(3.14,15);


	function getAverage(num1, num2, num3, num4) {

	
		let sum = num1 + num2 + num3 + num4;
		let average = sum / 4;

		console.log("the average of 20,50,60 and 80: " )
		console.log(average);
	}

	getAverage(20,40,60,80)



	function passingGrade(myScore, totalScore) {
			console.log("Is " + myScore + " / " + totalScore + " a passing score?");
			let averageScore = myScore/totalScore*100;
			let isPassed = averageScore > 75;
			console.log(isPassed);
		}

		passingGrade(38,50);